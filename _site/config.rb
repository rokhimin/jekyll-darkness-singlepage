

# config:
http_path = "/"
css_dir = "darkness/stylesheets"
sass_dir = "darkness/stylesheets"
images_dir = "darkness/images"
javascripts_dir = "darkness/javascripts"
output_style = :expanded
environment = :production
line_comments = false
color_output = false